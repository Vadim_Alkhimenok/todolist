const addTask = document.querySelector('#addTask');
const btnAdd = document.querySelector('#btnAdd');
const currentTasks = document.querySelector('#listCurrentTasks');
const sort = document.querySelector('.sort');
const increaseDate = document.querySelector('#btnSortIncreaseDate');
const decreaseDate = document.querySelector('#btnSortDecreaseDate');
const increaseAlphabet = document.querySelector('#btnSortIncreaseAlphabet');
const decreaseAlphabet = document.querySelector('#btnSortDecreaseAlphabet');
const btnSearch = document.querySelector('#btnSearch');
const btnReset = document.querySelector('#btnReset');
const error = document.querySelector('#error');
const inputTextFilter = document.querySelector('#inputTextFilter');
const inputDateFilter = document.querySelector('#inputDateFilter');

// global functions
const getCurrentTasks = () => [...currentTasks.querySelectorAll('.wrap')];
const setDisabledBtn = (btn, state) => btn.disabled = state;

// add new task
const getDataForm = () => ({
    text: document.forms[0]['inputTask'].value,
    date: document.forms[0]['inputDate'].value,
    id: Math.random(),
    isCross: false,
});

const setResetForm = () => ({
    text: document.forms[0]['inputTask'].value = '',
    date: document.forms[0]['inputDate'].value = '',
});

const templateTask = (text, date, id, isCross) => {
    currentTasks.insertAdjacentHTML('beforeend', `
        <div class='wrap' id=${id}>
            <li class="list-group-item d-flex w-100 m-3 task">
                <div class="w-100 mr-2">
                    <div class="d-flex w-100 justify-content-between flex-column h-100">
                        <div class="mb-1 w-100 text ${isCross ? 'cross' : ''}">${text}</div>
                        <hr>
                        <div class="date ${isCross ? 'cross' : ''}">${date}</div>
                    </div>
                </div>
                <div class="btns d-flex flex-column">
                    <button type="button" class="btn btn-success w-80 p-1 mb-1" id="btnComplete" ${isCross ? 'disabled' : ''}>Done</button>
                    <button type="button" class="btn btn-danger w-80 p-1 mb-1" id="btnDelete">Delete</button>
                </div>
            </li>
        </div>
    `);
};

btnAdd.addEventListener('click', () => {
    const { text, date, id, isCross } = getDataForm();

    templateTask(text, date, id, isCross);
    setResetForm();
    setTaskInLocalStorage({text, date, id, isCross})
});

// cross out task
const crossTask = tag => {
    let target = tag.closest('.wrap');
    let id = +target.id;

    setCrossTaskInLocalStorage(id);

    let task = target.querySelectorAll('.text, .date');
    task.forEach(task => task.classList.add('cross'));
}

const setDisableBtn = tag => {
    let target = tag.closest('.task').querySelector('#btnComplete');
    target.disabled = true;
}

// delete task
const deleteTask = tag => {
    let target = tag.closest('.wrap');

    let id = +target.id;
    deleteTaskFromLocalStorage(id);
    target.remove(); 
}

// properties of tasks
currentTasks.addEventListener('click' , event => {
    if (event.target.id === 'btnComplete') {
        crossTask(event.target);
        setDisableBtn(event.target);
    }

    if (event.target.id === 'btnDelete') {
        deleteTask(event.target)
    }
})

// sort
const sortTasks = (date, alphabet) => {
    const tasks = getCurrentTasks();

    if (date) {
        tasks.sort((a, b) => {
            let dateA = new Date(a.querySelector('.date').textContent);
            let dateB = new Date(b.querySelector('.date').textContent);
            return dateB - dateA;
        })
    } else if (alphabet) {
        tasks.sort((a, b) => {
            let wordA = a.querySelector('.text').textContent.toLowerCase().replace(/\s/g, '');
            let wordB = b.querySelector('.text').textContent.toLowerCase().replace(/\s/g, '');
            return wordA > wordB ? 1 : wordA < wordB ? -1 : 0;
        })
    } else {
        tasks.reverse()
    }
    tasks.forEach(li => currentTasks.insertAdjacentElement('beforeend', li));
}

const setDisabledSort = (increment, decrement) => {
    increment.disabled = true;
    decrement.disabled = false;
}

const startState = () => {
    decreaseDate.disabled = true;
    decreaseAlphabet.disabled = true;
}

sort.addEventListener('click', event => {    
    if (event.target.id === 'btnSortIncreaseDate') {
        setDisabledSort(increaseDate, decreaseDate);
        sortTasks(true, false);
    }

    if (event.target.id === 'btnSortDecreaseDate') {
        setDisabledSort(decreaseDate, increaseDate);
        sortTasks(false, false);
    }

    if (event.target.id === 'btnSortIncreaseAlphabet') {
        setDisabledSort(increaseAlphabet, decreaseAlphabet);
        sortTasks(false, true);
    }

    if (event.target.id === 'btnSortDecreaseAlphabet') {
        setDisabledSort(decreaseAlphabet, increaseAlphabet);
        sortTasks(false, false);
    }
})

startState();

// filter tasks
let filterData = {
    text: '',
    date: '',
}

inputTextFilter.addEventListener('change', ({target: {value}}) => {
    filterData.text = value;
    
    if (value === '') {
        setDisabledBtn(btnSearch, true);
    } else {
        setDisabledBtn(btnSearch, false);
    }
})

inputDateFilter.addEventListener('change', ({target: {value}}) => {
    filterData.date = value;

    if (value === '') {
        setDisabledBtn(btnSearch, true);
    } else {
        setDisabledBtn(btnSearch, false);
    }
})

setDisabledBtn(btnSearch, true);

btnReset.addEventListener('click', () => {
    const tasks = getCurrentTasks();

    filterData = {
        text: '',
        date: ''
    }

    inputDateFilter.value = '';
    inputTextFilter.value = '';

    tasks.forEach(task => {
        task.classList.remove('d-none');
        task.classList.remove('d-block')
    });

    error.classList.add('hide');    
})

btnSearch.addEventListener('click', () => {
    const tasks = getCurrentTasks();
    let { text, date } = filterData;
    let counterErrors = 0;

    tasks.forEach(task => {
        let tempTask = task.querySelector('.text').textContent.toLowerCase().replace(/\s/g, '');
        let tempTaskDate = task.querySelector('.date').textContent;
        text = text.toLowerCase().replace(/\s/g, '');

        if (text !== '' && date !== '') {
            if (tempTask.includes(text) && tempTaskDate === date) {
                task.classList.remove('d-none')
                task.classList.add('d-block');
            } else {
                task.classList.remove('d-block');
                task.classList.add('d-none')
                counterErrors++;
            }
        } else {
            if (tempTask.includes(text) && text !== '') {
                task.classList.remove('d-none')
                task.classList.add('d-block')
            } else if (tempTaskDate === date) {
                task.classList.remove('d-none')
                task.classList.add('d-block')
            } else {
                task.classList.remove('d-block');
                task.classList.add('d-none');
                counterErrors++;
            }
        }

        if (counterErrors == tasks.length) {
            error.classList.remove('hide')
        }
    })
})

// localStorage
let tasks = [];
let tasksFromLocalStorage = JSON.parse(localStorage.getItem('tasks'));

if (tasksFromLocalStorage) {
    tasks = [...tasksFromLocalStorage];
    tasksFromLocalStorage.map(task => templateTask(task.text, task.date, task.id, task.isCross))
}

const setTaskInLocalStorage = task => {
    tasks.push(task);
    localStorage.setItem('tasks', JSON.stringify(tasks))
}

const deleteTaskFromLocalStorage = id => {
    let taskIndex = tasks.findIndex(task => task.id === id);
    tasks.splice(taskIndex, 1);
    localStorage.setItem('tasks', JSON.stringify(tasks))
}

const setCrossTaskInLocalStorage = id => {
    tasks.map(task => task.id === id ? task.isCross = true : null);
    localStorage.setItem('tasks', JSON.stringify(tasks))
}
